# xdames

XLib French checkers written in C by Alain Danger.

Homepage of the original project: https://sourceforge.net/projects/xdames/

![Screenshot](screenshot.png)
