
typedef struct
{
  int dep_x;
  int dep_y;
  int arr_x;
  int arr_y;
  int pr_x;
  int pr_y;
  int prise;
  int prec;
  int ev_pos;
} LISTE_MVT;

extern Window win;
extern Display* XDisp;
void rafraichit(void);
//int signe(int x);
int init_fenetre(void);
void detruit_fenetre(void);
void efface_fenetre(int couleur);
//void affiche_damier(void);
void dessine_pion(int x, int y, int c);
void dessine_dame(int x, int y, int c);
//void affiche_pions(int TabJeu[12][12]);
void affiche_tout(int TabJeu[12][12]);
void anim_pion(int p, int i, int j, int k, int l, int TabJeu[12][12]);
void affiche_mvt_b(LISTE_MVT mvt_b[256], int ptr_mvt_b[1]);
