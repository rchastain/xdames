
#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "xdames-disp.h"

#define PION_B 1
#define PION_N 2
#define DAME_B 5
#define DAME_N 6
#define COL_B 248
#define COL_N 7

LISTE_MVT mvt_tmp[256];
int ptr_mvt_tmp[1];
int prise_max;
int niveau = 1;

int hasard(int x)
{
  return (int)((double)x * (double)rand() / (double)RAND_MAX);
}

int eval_pos(int TabJeu[12][12])
{
  int i, j, x;
  x = 0;

  for (i = 1; i <= 10; i++)
  {
    for (j = 1; j <= 10; j++)
    {
      switch (TabJeu[i][j])
      {
      case PION_B:
        x = x + 20 + (i - 1);
        break;

      case DAME_B:
        x = x + 100;
        break;

      case PION_N:
        x = x - 20 - (10 - i);
        break;

      case DAME_N:
        x = x - 100;
        break;
      };
    };
  };

  return x;
};

void cherche_mvt(int couleur, int TabJeu[12][12])
{
  int i, j, k;
  int frd1, frd2;

  if ((couleur != PION_B) && (couleur != PION_N) && (couleur != DAME_B) && (couleur != DAME_N))
  {
    fprintf(stderr, "Erreur : recherche de mouvement sans définition de couleur\n");
    return;
  }

  if ((couleur == PION_B) || (couleur == DAME_B))
  {
    frd1 = PION_B;
    frd2 = DAME_B;
  }
  else
  {
    if ((couleur == PION_N) || (couleur == DAME_N))
    {
      frd1 = PION_N;
      frd2 = DAME_N;
    }
    else
    {
      fprintf(stderr, "Erreur : recherche de mouvement sans définition de couleur\n");
      return;
    };
  };

  for (i = 1; i <= 10; i++)
  {
    for (j = 1; j <= 10; j++)
    {
      if ((i < 10) && (j > 1) && (TabJeu[i][j] == frd1) && (TabJeu[i + 1][j - 1] == 0) && (frd1 == PION_N))
      {
        ptr_mvt_tmp[0]++;
        mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
        mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
        mvt_tmp[ptr_mvt_tmp[0]].arr_x = i + 1;
        mvt_tmp[ptr_mvt_tmp[0]].arr_y = j - 1;
        mvt_tmp[ptr_mvt_tmp[0]].pr_x = 0;
        mvt_tmp[ptr_mvt_tmp[0]].pr_y = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prise = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prec = 0;
        TabJeu[i][j] = 0;
        TabJeu[i + 1][j - 1] = frd1;
        mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TabJeu);
        TabJeu[i][j] = frd1;
        TabJeu[i + 1][j - 1] = 0;
      };

      if ((i > 1) && (j > 1) && (TabJeu[i][j] == frd1) && (TabJeu[i - 1][j - 1] == 0) && (frd1 == PION_N))
      {
        ptr_mvt_tmp[0]++;
        mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
        mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
        mvt_tmp[ptr_mvt_tmp[0]].arr_x = i - 1;
        mvt_tmp[ptr_mvt_tmp[0]].arr_y = j - 1;
        mvt_tmp[ptr_mvt_tmp[0]].pr_x = 0;
        mvt_tmp[ptr_mvt_tmp[0]].pr_y = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prise = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prec = 0;
        TabJeu[i][j] = 0;
        TabJeu[i - 1][j - 1] = frd1;
        mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TabJeu);
        TabJeu[i][j] = frd1;
        TabJeu[i - 1][j - 1] = 0;
      };

      if ((i < 10) && (j < 10) && (TabJeu[i][j] == frd1) && (TabJeu[i + 1][j + 1] == 0) && (frd1 == PION_B))
      {
        ptr_mvt_tmp[0]++;
        mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
        mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
        mvt_tmp[ptr_mvt_tmp[0]].arr_x = i + 1;
        mvt_tmp[ptr_mvt_tmp[0]].arr_y = j + 1;
        mvt_tmp[ptr_mvt_tmp[0]].pr_x = 0;
        mvt_tmp[ptr_mvt_tmp[0]].pr_y = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prise = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prec = 0;
        TabJeu[i][j] = 0;
        TabJeu[i + 1][j + 1] = frd1;
        mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TabJeu);
        TabJeu[i][j] = frd1;
        TabJeu[i + 1][j + 1] = 0;
      };

      if ((i > 1) && (j < 10) && (TabJeu[i][j] == frd1) && (TabJeu[i - 1][j + 1] == 0) && (frd1 == PION_B))
      {
        ptr_mvt_tmp[0]++;
        mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
        mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
        mvt_tmp[ptr_mvt_tmp[0]].arr_x = i - 1;
        mvt_tmp[ptr_mvt_tmp[0]].arr_y = j + 1;
        mvt_tmp[ptr_mvt_tmp[0]].pr_x = 0;
        mvt_tmp[ptr_mvt_tmp[0]].pr_y = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prise = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prec = 0;
        TabJeu[i][j] = 0;
        TabJeu[i - 1][j + 1] = frd1;
        mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TabJeu);
        TabJeu[i][j] = frd1;
        TabJeu[i - 1][j + 1] = 0;
      };

      k = 1;

      while ((i + k <= 10) && (j - k >= 1) && (TabJeu[i][j] == frd2) && (TabJeu[i + k][j - k] == 0))
      {
        ptr_mvt_tmp[0]++;
        mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
        mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
        mvt_tmp[ptr_mvt_tmp[0]].arr_x = i + k;
        mvt_tmp[ptr_mvt_tmp[0]].arr_y = j - k;
        mvt_tmp[ptr_mvt_tmp[0]].pr_x = 0;
        mvt_tmp[ptr_mvt_tmp[0]].pr_y = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prise = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prec = 0;
        TabJeu[i][j] = 0;
        TabJeu[i + k][j - k] = frd2;
        mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TabJeu);
        TabJeu[i][j] = frd2;
        TabJeu[i + k][j - k] = 0;
        k++;
      };

      k = 1;

      while ((i - k >= 1) && (j - k >= 1) && (TabJeu[i][j] == frd2) && (TabJeu[i - k][j - k] == 0))
      {
        ptr_mvt_tmp[0]++;
        mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
        mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
        mvt_tmp[ptr_mvt_tmp[0]].arr_x = i - k;
        mvt_tmp[ptr_mvt_tmp[0]].arr_y = j - k;
        mvt_tmp[ptr_mvt_tmp[0]].pr_x = 0;
        mvt_tmp[ptr_mvt_tmp[0]].pr_y = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prise = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prec = 0;
        TabJeu[i][j] = 0;
        TabJeu[i - k][j - k] = frd2;
        mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TabJeu);
        TabJeu[i][j] = frd2;
        TabJeu[i - k][j - k] = 0;
        k++;
      };

      k = 1;

      while ((i - k >= 1) && (j + k <= 10) && (TabJeu[i][j] == frd2) && (TabJeu[i - k][j + k] == 0))
      {
        ptr_mvt_tmp[0]++;
        mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
        mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
        mvt_tmp[ptr_mvt_tmp[0]].arr_x = i - k;
        mvt_tmp[ptr_mvt_tmp[0]].arr_y = j + k;
        mvt_tmp[ptr_mvt_tmp[0]].pr_x = 0;
        mvt_tmp[ptr_mvt_tmp[0]].pr_y = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prise = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prec = 0;
        TabJeu[i][j] = 0;
        TabJeu[i - k][j + k] = frd2;
        mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TabJeu);
        TabJeu[i][j] = frd2;
        TabJeu[i - k][j + k] = 0;
        k++;
      };

      k = 1;

      while ((i + k <= 10) && (j + k <= 10) && (TabJeu[i][j] == frd2) && (TabJeu[i + k][j + k] == 0))
      {
        ptr_mvt_tmp[0]++;
        mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
        mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
        mvt_tmp[ptr_mvt_tmp[0]].arr_x = i + k;
        mvt_tmp[ptr_mvt_tmp[0]].arr_y = j + k;
        mvt_tmp[ptr_mvt_tmp[0]].pr_x = 0;
        mvt_tmp[ptr_mvt_tmp[0]].pr_y = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prise = 0;
        mvt_tmp[ptr_mvt_tmp[0]].prec = 0;
        TabJeu[i][j] = 0;
        TabJeu[i + k][j + k] = frd2;
        mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TabJeu);
        TabJeu[i][j] = frd2;
        TabJeu[i + k][j + k] = 0;
        k++;
      };
    };
  };
}

void cherche_prise_dame(int couleur, int i, int j, int nbprise, int pred, int TempTJ[12][12])
{
  int k, l, x, y;
  int TTJ[12][12];
  int foe1, foe2;

  if ((couleur == PION_B) || (couleur == DAME_B))
  {
    foe1 = PION_N;
    foe2 = DAME_N;
  }
  else
  {
    if ((couleur == PION_N) || (couleur == DAME_N))
    {
      foe1 = PION_B;
      foe2 = DAME_B;
    }
    else
    {
      fprintf(stderr, "Erreur : recherche de mouvement sans définition de couleur\n");
      return;
    };
  };

  k = 1;

  while (((i + k) <= 8) && ((j + k) <= 8) && (TempTJ[i + k][j + k] == 0))
    k++;

  if ((TempTJ[i + k][j + k] == foe1) || (TempTJ[i + k][j + k] == foe2))
  {
    l = k + 1;

    while (((i + l) <= 10) && ((j + l) <= 10) && (TempTJ[i + l][j + l] == 0))
    {
      ptr_mvt_tmp[0]++;
      mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
      mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
      mvt_tmp[ptr_mvt_tmp[0]].arr_x = i + l;
      mvt_tmp[ptr_mvt_tmp[0]].arr_y = j + l;
      mvt_tmp[ptr_mvt_tmp[0]].pr_x = i + k;
      mvt_tmp[ptr_mvt_tmp[0]].pr_y = j + k;
      mvt_tmp[ptr_mvt_tmp[0]].prise = nbprise + 1;
      mvt_tmp[ptr_mvt_tmp[0]].prec = pred;

      if (nbprise + 1 > prise_max)
        prise_max = nbprise + 1;

      for (x = 0; x < 12; x++)
        for (y = 0; y < 12; y++)
          TTJ[x][y] = TempTJ[x][y];

      TTJ[i + l][j + l] = TempTJ[i][j];
      TTJ[i][j] = 0;
      TTJ[i + k][j + k] = 0;
      mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TTJ);
      cherche_prise_dame(couleur, i + l, j + l, nbprise + 1, ptr_mvt_tmp[0], TTJ);
      l++;
    };
  };

  k = 1;

  while (((i - k) >= 3) && ((j - k) >= 3) && (TempTJ[i - k][j - k] == 0))
    k++;

  if ((TempTJ[i - k][j - k] == foe1) || (TempTJ[i - k][j - k] == foe2))
  {
    l = k + 1;

    while (((i - l) >= 1) && ((j - l) >= 1) && (TempTJ[i - l][j - l] == 0))
    {
      ptr_mvt_tmp[0]++;
      mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
      mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
      mvt_tmp[ptr_mvt_tmp[0]].arr_x = i - l;
      mvt_tmp[ptr_mvt_tmp[0]].arr_y = j - l;
      mvt_tmp[ptr_mvt_tmp[0]].pr_x = i - k;
      mvt_tmp[ptr_mvt_tmp[0]].pr_y = j - k;
      mvt_tmp[ptr_mvt_tmp[0]].prise = nbprise + 1;
      mvt_tmp[ptr_mvt_tmp[0]].prec = pred;

      if (nbprise + 1 > prise_max)
        prise_max = nbprise + 1;

      for (x = 0; x < 12; x++)
        for (y = 0; y < 12; y++)
          TTJ[x][y] = TempTJ[x][y];

      TTJ[i - l][j - l] = TempTJ[i][j];
      TTJ[i][j] = 0;
      TTJ[i - k][j - k] = 0;
      mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TTJ);
      cherche_prise_dame(couleur, i - l, j - l, nbprise + 1, ptr_mvt_tmp[0], TTJ);
      l++;
    };
  };

  k = 1;

  while (((i + k) <= 8) && ((j - k) >= 3) && (TempTJ[i + k][j - k] == 0))
    k++;

  if ((TempTJ[i + k][j - k] == foe1) || (TempTJ[i + k][j - k] == foe2))
  {
    l = k + 1;

    while (((i + l) <= 10) && ((j - l) >= 1) && (TempTJ[i + l][j - l] == 0))
    {
      ptr_mvt_tmp[0]++;
      mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
      mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
      mvt_tmp[ptr_mvt_tmp[0]].arr_x = i + l;
      mvt_tmp[ptr_mvt_tmp[0]].arr_y = j - l;
      mvt_tmp[ptr_mvt_tmp[0]].pr_x = i + k;
      mvt_tmp[ptr_mvt_tmp[0]].pr_y = j - k;
      mvt_tmp[ptr_mvt_tmp[0]].prise = nbprise + 1;
      mvt_tmp[ptr_mvt_tmp[0]].prec = pred;

      if (nbprise + 1 > prise_max)
        prise_max = nbprise + 1;

      for (x = 0; x < 12; x++)
        for (y = 0; y < 12; y++)
          TTJ[x][y] = TempTJ[x][y];

      TTJ[i + l][j - l] = TempTJ[i][j];
      TTJ[i][j] = 0;
      TTJ[i + k][j - k] = 0;
      mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TTJ);
      cherche_prise_dame(couleur, i + l, j - l, nbprise + 1, ptr_mvt_tmp[0], TTJ);
      l++;
    };
  };

  k = 1;

  while (((i - k) >= 3) && ((j + k) <= 8) && (TempTJ[i - k][j + k] == 0))
    k++;

  if ((TempTJ[i - k][j + k] == foe1) || (TempTJ[i - k][j + k] == foe2))
  {
    l = k + 1;

    while (((i - l) >= 1) && ((j + l) <= 10) && (TempTJ[i - l][j + l] == 0))
    {
      ptr_mvt_tmp[0]++;
      mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
      mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
      mvt_tmp[ptr_mvt_tmp[0]].arr_x = i - l;
      mvt_tmp[ptr_mvt_tmp[0]].arr_y = j + l;
      mvt_tmp[ptr_mvt_tmp[0]].pr_x = i - k;
      mvt_tmp[ptr_mvt_tmp[0]].pr_y = j + k;
      mvt_tmp[ptr_mvt_tmp[0]].prise = nbprise + 1;
      mvt_tmp[ptr_mvt_tmp[0]].prec = pred;

      if (nbprise + 1 > prise_max)
        prise_max = nbprise + 1;

      for (x = 0; x < 12; x++)
        for (y = 0; y < 12; y++)
          TTJ[x][y] = TempTJ[x][y];

      TTJ[i - l][j + l] = TempTJ[i][j];
      TTJ[i][j] = 0;
      TTJ[i - k][j + k] = 0;
      mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TTJ);
      cherche_prise_dame(couleur, i - l, j + l, nbprise + 1, ptr_mvt_tmp[0], TTJ);
      l++;
    };
  };
}

void cherche_prise_pion(int couleur, int i, int j, int nbprise, int pred, int TempTJ[2][12])
{
  int k, l;
  int TTJ[12][12];
  int foe1, foe2;

  if ((couleur == PION_B) || (couleur == DAME_B))
  {
    foe1 = PION_N;
    foe2 = DAME_N;
  }
  else
  {
    if ((couleur == PION_N) || (couleur == DAME_N))
    {
      foe1 = PION_B;
      foe2 = DAME_B;
    }
    else
    {
      fprintf(stderr, "Erreur : recherche de mouvement sans définition de couleur\n");
      return;
    };
  };

  if ((i < 9) && (j < 9) && ((TempTJ[i + 1][j + 1] == foe1) || (TempTJ[i + 1][j + 1] == foe2)) && (TempTJ[i + 2][j + 2] == 0))
  {
    ptr_mvt_tmp[0]++;
    mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
    mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
    mvt_tmp[ptr_mvt_tmp[0]].arr_x = i + 2;
    mvt_tmp[ptr_mvt_tmp[0]].arr_y = j + 2;
    mvt_tmp[ptr_mvt_tmp[0]].pr_x = i + 1;
    mvt_tmp[ptr_mvt_tmp[0]].pr_y = j + 1;
    mvt_tmp[ptr_mvt_tmp[0]].prise = nbprise + 1;

    if (nbprise + 1 > prise_max)
      prise_max = nbprise + 1;

    mvt_tmp[ptr_mvt_tmp[0]].prec = pred;

    for (k = 0; k < 12; k++)
      for (l = 0; l < 12; l++)
        TTJ[k][l] = TempTJ[k][l];

    TTJ[i + 2][j + 2] = TempTJ[i][j];
    TTJ[i][j] = 0;
    TTJ[i + 1][j + 1] = 0;
    mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TTJ);
    cherche_prise_pion(couleur, i + 2, j + 2, nbprise + 1, ptr_mvt_tmp[0], TTJ);
  };

  if ((i < 9) && (j > 2) && ((TempTJ[i + 1][j - 1] == foe1) || (TempTJ[i + 1][j - 1] == foe2)) && (TempTJ[i + 2][j - 2] == 0))
  {
    ptr_mvt_tmp[0]++;
    mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
    mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
    mvt_tmp[ptr_mvt_tmp[0]].arr_x = i + 2;
    mvt_tmp[ptr_mvt_tmp[0]].arr_y = j - 2;
    mvt_tmp[ptr_mvt_tmp[0]].pr_x = i + 1;
    mvt_tmp[ptr_mvt_tmp[0]].pr_y = j - 1;
    mvt_tmp[ptr_mvt_tmp[0]].prise = nbprise + 1;

    if (nbprise + 1 > prise_max)
      prise_max = nbprise + 1;

    mvt_tmp[ptr_mvt_tmp[0]].prec = pred;

    for (k = 0; k < 12; k++)
      for (l = 0; l < 12; l++)
        TTJ[k][l] = TempTJ[k][l];

    TTJ[i + 2][j - 2] = TempTJ[i][j];
    TTJ[i][j] = 0;
    TTJ[i + 1][j - 1] = 0;
    mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TTJ);
    cherche_prise_pion(couleur, i + 2, j - 2, nbprise + 1, ptr_mvt_tmp[0], TTJ);
  };

  if ((i > 2) && (j < 9) && ((TempTJ[i - 1][j + 1] == foe1) || (TempTJ[i - 1][j + 1] == foe2)) && (TempTJ[i - 2][j + 2] == 0))
  {
    ptr_mvt_tmp[0]++;
    mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
    mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
    mvt_tmp[ptr_mvt_tmp[0]].arr_x = i - 2;
    mvt_tmp[ptr_mvt_tmp[0]].arr_y = j + 2;
    mvt_tmp[ptr_mvt_tmp[0]].pr_x = i - 1;
    mvt_tmp[ptr_mvt_tmp[0]].pr_y = j + 1;
    mvt_tmp[ptr_mvt_tmp[0]].prise = nbprise + 1;

    if (nbprise + 1 > prise_max)
      prise_max = nbprise + 1;

    mvt_tmp[ptr_mvt_tmp[0]].prec = pred;

    for (k = 0; k < 12; k++)
      for (l = 0; l < 12; l++)
        TTJ[k][l] = TempTJ[k][l];

    TTJ[i - 2][j + 2] = TempTJ[i][j];
    TTJ[i][j] = 0;
    TTJ[i - 1][j + 1] = 0;
    mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TTJ);
    cherche_prise_pion(couleur, i - 2, j + 2, nbprise + 1, ptr_mvt_tmp[0], TTJ);
  };

  if ((i > 2) && (j > 2) && ((TempTJ[i - 1][j - 1] == foe1) || (TempTJ[i - 1][j - 1] == foe2)) && (TempTJ[i - 2][j - 2] == 0))
  {
    ptr_mvt_tmp[0]++;
    mvt_tmp[ptr_mvt_tmp[0]].dep_x = i;
    mvt_tmp[ptr_mvt_tmp[0]].dep_y = j;
    mvt_tmp[ptr_mvt_tmp[0]].arr_x = i - 2;
    mvt_tmp[ptr_mvt_tmp[0]].arr_y = j - 2;
    mvt_tmp[ptr_mvt_tmp[0]].pr_x = i - 1;
    mvt_tmp[ptr_mvt_tmp[0]].pr_y = j - 1;
    mvt_tmp[ptr_mvt_tmp[0]].prise = nbprise + 1;

    if (nbprise + 1 > prise_max)
      prise_max = nbprise + 1;

    mvt_tmp[ptr_mvt_tmp[0]].prec = pred;

    for (k = 0; k < 12; k++)
      for (l = 0; l < 12; l++)
        TTJ[k][l] = TempTJ[k][l];

    TTJ[i - 2][j + 2] = TempTJ[i][j];
    TTJ[i][j] = 0;
    TTJ[i - 1][j - 1] = 0;
    mvt_tmp[ptr_mvt_tmp[0]].ev_pos = eval_pos(TTJ);
    cherche_prise_pion(couleur, i - 2, j - 2, nbprise + 1, ptr_mvt_tmp[0], TTJ);
  };
}

void efface_tab_mvt(LISTE_MVT mvt_dummy[256], int ptr_mvt_dummy[1])
{
  int i;

  for (i = 255; i >= 0; i--)
  {
    mvt_dummy[i].dep_x = 0;
    mvt_dummy[i].dep_y = 0;
    mvt_dummy[i].arr_x = 0;
    mvt_dummy[i].arr_y = 0;
    mvt_dummy[i].pr_x = 0;
    mvt_dummy[i].pr_y = 0;
    mvt_dummy[i].prise = 0;
    mvt_dummy[i].prec = 0;
    mvt_dummy[i].ev_pos = 0;
  };

  ptr_mvt_dummy[0] = 0;
}

void cherche_coup(int couleur, int TabJeu[12][12], LISTE_MVT mvt_dummy[256], int ptr_mvt_dummy[1])
{
  efface_tab_mvt(mvt_tmp, ptr_mvt_tmp);
  int i, j, l;
  int frd1, frd2;

  if ((couleur == PION_B) || (couleur == DAME_B))
  {
    frd1 = PION_B;
    frd2 = DAME_B;
  }
  else
  {
    if ((couleur == PION_N) || (couleur == DAME_N))
    {
      frd1 = PION_N;
      frd2 = DAME_N;
    }
    else
    {
      fprintf(stderr, "Erreur : recherche de mouvement sans définition de couleur\n");
      return;
    };
  };

  prise_max = 0;

  for (i = 1; i <= 10; i++)
  {
    for (j = 1; j <= 10; j++)
    {
      if (TabJeu[i][j] == frd1)
        cherche_prise_pion(frd1, i, j, 0, 0, TabJeu);

      if (TabJeu[i][j] == frd2)
        cherche_prise_dame(frd2, i, j, 0, 0, TabJeu);
    };
  };

  if (prise_max == 0)
  {
    cherche_mvt(frd1, TabJeu);

    for (i = 1; i <= ptr_mvt_tmp[0]; i++)
    {
      mvt_dummy[i].dep_x = mvt_tmp[i].dep_x;
      mvt_dummy[i].dep_y = mvt_tmp[i].dep_y;
      mvt_dummy[i].arr_x = mvt_tmp[i].arr_x;
      mvt_dummy[i].arr_y = mvt_tmp[i].arr_y;
      mvt_dummy[i].pr_x = mvt_tmp[i].pr_x;
      mvt_dummy[i].pr_y = mvt_tmp[i].pr_y;
      mvt_dummy[i].prec = mvt_tmp[i].prec;
      mvt_dummy[i].prise = mvt_tmp[i].prise;
      mvt_dummy[i].ev_pos = mvt_tmp[i].ev_pos;
    };

    ptr_mvt_dummy[0] = ptr_mvt_tmp[0];
  }
  else
  {
    ptr_mvt_dummy[0] = 0;

    for (i = 1; i <= ptr_mvt_tmp[0]; i++)
    {
      if (mvt_tmp[i].prise == prise_max)
      {
        l = i;

        while (l != 0)
        {
          ptr_mvt_dummy[0]++;
          mvt_dummy[ptr_mvt_dummy[0]].dep_x = mvt_tmp[l].dep_x;
          mvt_dummy[ptr_mvt_dummy[0]].dep_y = mvt_tmp[l].dep_y;
          mvt_dummy[ptr_mvt_dummy[0]].arr_x = mvt_tmp[l].arr_x;
          mvt_dummy[ptr_mvt_dummy[0]].arr_y = mvt_tmp[l].arr_y;
          mvt_dummy[ptr_mvt_dummy[0]].pr_x = mvt_tmp[l].pr_x;
          mvt_dummy[ptr_mvt_dummy[0]].pr_y = mvt_tmp[l].pr_y;
          /*
          mvt_dummy[ptr_mvt_dummy[0]].prec=mvt_tmp[l].prec;
          */
          mvt_dummy[ptr_mvt_dummy[0]].prise = mvt_tmp[l].prise;
          mvt_dummy[ptr_mvt_dummy[0]].ev_pos = mvt_tmp[l].ev_pos;
          l = mvt_tmp[l].prec;

          if (l == 0)
            mvt_dummy[ptr_mvt_dummy[0]].prec = 0;
          else
            mvt_dummy[ptr_mvt_dummy[0]].prec = ptr_mvt_dummy[0] + 1;
        };
      };
    };
  };
}

void init_tabjeu(int TabJeu[12][12])
{
  int i, j;

  for (i = 0; i < 12; i++)
  {
    for (j = 0; j < 12; j++)
    {
      TabJeu[i][j] = 0;
    };
  };

  for (i = 1; i <= 10; i++)
  {
    for (j = 1; j <= 4; j++)
    {
      if (((i + j) & 1) == 0)
      {
        TabJeu[i][j] = PION_B;
        TabJeu[i][j + 6] = PION_N;
      };
    };
  };
}

void partie_perdue()
{
  fprintf(stderr, "VOUS AVEZ PERDU !\n");
  getchar();
}

void partie_gagnee()
{
  fprintf(stderr, "VOUS AVEZ GAGNÉ, BRAVO !\n");
  getchar();
}

int joue_b_interactif(int TabJeu[12][12], LISTE_MVT mvt_b[256], int ptr_mvt_b[1])
{
  int i, j, k, l, p, x, u, v;
  int valide = False;
  int mvt_ok = False;
  XEvent report;
  cherche_coup(PION_B, TabJeu, mvt_b, ptr_mvt_b);

  if (ptr_mvt_b[0] == 0)
  {
    partie_perdue();
    return 1;
  };

  XSelectInput(XDisp, win, ExposureMask | KeyPressMask | ButtonPressMask | ButtonReleaseMask | ButtonMotionMask);

  do
  {
    if (niveau == 0)
      affiche_mvt_b(mvt_b, ptr_mvt_b);

    do
    {
      XNextEvent(XDisp, &report);

      switch (report.type)
      {
      case Expose:
        affiche_tout(TabJeu);

        if (niveau == 0)
          affiche_mvt_b(mvt_b, ptr_mvt_b);

        break;

      case ButtonPress:
        affiche_tout(TabJeu);
        i = (int)(report.xbutton.x / 40);
        j = 11 - (int)(report.xbutton.y / 40);

        if ((i >= 1) && (i <= 10) && (j >= 1) && (j <= 10) && ((TabJeu[i][j] == PION_B) || (TabJeu[i][j] == DAME_B)))
        {
          for (x = 1; (x <= ptr_mvt_b[0]) && (valide != True); x++)
            if ((mvt_b[x].dep_x == i) && (mvt_b[x].dep_y == j))
              valide = True;
        };

        if ((valide != True) && (niveau == 0))
          affiche_mvt_b(mvt_b, ptr_mvt_b);

        break;

      default:
        break;
      };
    }
    while (valide != True);
    
    valide = False;
    p = TabJeu[i][j];
    TabJeu[i][j] = 0;

    do
    {
      XNextEvent(XDisp, &report);

      switch (report.type)
      {
      case Expose:
        affiche_tout(TabJeu);

        if (niveau == 0)
          affiche_mvt_b(mvt_b, ptr_mvt_b);

        break;

      case ButtonRelease:
        valide = True;
        mvt_ok = False;
        affiche_tout(TabJeu);
        k = (int)(report.xbutton.x / 40);
        l = 11 - (int)(report.xbutton.y / 40);

        if ((k >= 1) && (k <= 10) && (l >= 1) && (l <= 10) && (TabJeu[k][l] == 0))
          for (x = 1; (x <= ptr_mvt_b[0]) && (mvt_ok != True); x++)
          {
            if ((mvt_b[x].arr_x == k) && (mvt_b[x].arr_y == l) && (mvt_b[x].dep_x == i) && (mvt_b[x].dep_y == j))
            {
              u = mvt_b[x].pr_x;
              v = mvt_b[x].pr_y;
              mvt_b[x].dep_x = 0;
              mvt_b[x].dep_y = 0;
              mvt_b[x].arr_x = 0;
              mvt_b[x].arr_y = 0;
              mvt_ok = True;
            };
          };

        break;

      case MotionNotify:
        affiche_tout(TabJeu);

        if (niveau == 0)
          affiche_mvt_b(mvt_b, ptr_mvt_b);

        if (p == PION_B)
          dessine_pion(report.xbutton.x, report.xbutton.y, 248);
        else
          dessine_dame(report.xbutton.x, report.xbutton.y, 248);

        break;

      default:
        break;
      };
    }
    while (valide != True);

    if (mvt_ok != True)
    {
      TabJeu[i][j] = p;
      affiche_tout(TabJeu);

      if (niveau == 0)
        affiche_mvt_b(mvt_b, ptr_mvt_b);
    };
  }
  while (mvt_ok != True);

  prise_max--;
  TabJeu[k][l] = p;
  TabJeu[u][v] = 0;
  affiche_tout(TabJeu);

  if (niveau == 0)
    affiche_mvt_b(mvt_b, ptr_mvt_b);

  while (prise_max > 0)
  {
    do
    {
      if (niveau == 0)
        affiche_mvt_b(mvt_b, ptr_mvt_b);

      do
      {
        XNextEvent(XDisp, &report);

        switch (report.type)
        {
        case Expose:
          affiche_tout(TabJeu);

          if (niveau == 0)
            affiche_mvt_b(mvt_b, ptr_mvt_b);

          break;

        case ButtonPress:
          affiche_tout(TabJeu);
          i = (int)(report.xbutton.x / 40);
          j = 11 - (int)(report.xbutton.y / 40);
          valide = False;

          if ((i == k) && (j == l))
          {
            valide = True;
          };

          if ((valide != True) && (niveau == 0))
            affiche_mvt_b(mvt_b, ptr_mvt_b);

          break;

        default:
          break;
        };
      }
      while (valide != True);
      
      valide = False;
      p = TabJeu[i][j];
      TabJeu[i][j] = 0;

      do
      {
        XNextEvent(XDisp, &report);

        switch (report.type)
        {
        case Expose:
          affiche_tout(TabJeu);

          if (niveau == 0)
            affiche_mvt_b(mvt_b, ptr_mvt_b);

          break;

        case ButtonRelease:
          valide = True;
          mvt_ok = False;
          affiche_tout(TabJeu);
          k = (int)(report.xbutton.x / 40);
          l = 11 - (int)(report.xbutton.y / 40);

          if ((k >= 1) && (k <= 10) && (l >= 1) && (l <= 10) && (TabJeu[k][l] == 0))
            for (x = 1; (x <= ptr_mvt_b[0]) && (mvt_ok != True); x++)
            {
              if ((mvt_b[x].arr_x == k) && (mvt_b[x].arr_y == l) && (mvt_b[x].dep_x == i) && (mvt_b[x].dep_y == j))
              {
                u = mvt_b[x].pr_x;
                v = mvt_b[x].pr_y;
                mvt_b[x].dep_x = 0;
                mvt_b[x].dep_y = 0;
                mvt_b[x].arr_x = 0;
                mvt_b[x].arr_y = 0;
                mvt_ok = True;
              };
            };

          break;

        case MotionNotify:
          affiche_tout(TabJeu);

          if (niveau == 0)
            affiche_mvt_b(mvt_b, ptr_mvt_b);

          if (p == PION_B)
            dessine_pion(report.xbutton.x, report.xbutton.y, 248);
          else
            dessine_dame(report.xbutton.x, report.xbutton.y, 248);

          break;

        default:
          break;
        };
      }
      while (valide != True);

      if (mvt_ok != True)
      {
        TabJeu[i][j] = p;
        affiche_tout(TabJeu);

        if (niveau == 0)
          affiche_mvt_b(mvt_b, ptr_mvt_b);
      };
    }
    while (mvt_ok != True);

    prise_max--;
    TabJeu[k][l] = p;
    TabJeu[u][v] = 0;
    affiche_tout(TabJeu);

    if (niveau == 0)
      affiche_mvt_b(mvt_b, ptr_mvt_b);
  };

  if (l == 10)
    TabJeu[k][l] = DAME_B;

  return 0;
}

void joue_coup(int TabJeu[12][12], LISTE_MVT mvt_dummy[256], int i, int anim)
{
  int p;

  if (mvt_dummy[i].prec != 0)
  {
    joue_coup(TabJeu, mvt_dummy, mvt_dummy[i].prec, anim);
  };

  p = TabJeu[mvt_dummy[i].dep_x][mvt_dummy[i].dep_y];

  TabJeu[mvt_dummy[i].dep_x][mvt_dummy[i].dep_y] = 0;

  if (anim != False)
  {
    if ((mvt_dummy[i].pr_x != 0) && (mvt_dummy[i].pr_y != 0))
    {
      anim_pion(p, mvt_dummy[i].dep_x, mvt_dummy[i].dep_y, mvt_dummy[i].pr_x, mvt_dummy[i].pr_y, TabJeu);
      TabJeu[mvt_dummy[i].pr_x][mvt_dummy[i].pr_y] = 0;
      anim_pion(p, mvt_dummy[i].pr_x, mvt_dummy[i].pr_y, mvt_dummy[i].arr_x, mvt_dummy[i].arr_y, TabJeu);
    }
    else
    {
      anim_pion(p, mvt_dummy[i].dep_x, mvt_dummy[i].dep_y, mvt_dummy[i].arr_x, mvt_dummy[i].arr_y, TabJeu);
    };
  };

  TabJeu[mvt_dummy[i].arr_x][mvt_dummy[i].arr_y] = p;
};

int joue_n0(int TabJeu[12][12], LISTE_MVT mvt_n[256], int ptr_mvt_n[1])
{
  int x;
  cherche_coup(PION_N, TabJeu, mvt_n, ptr_mvt_n);

  if (ptr_mvt_n[0] == 0)
  {
    partie_gagnee();
    return 1;
  };

  x = 0;

  do
  {
    x = hasard(ptr_mvt_n[0]) + 1;
    fprintf(stderr, "Le hasard donne %d pour %d avec %d\n", x, ptr_mvt_n[0], prise_max);
  }
  while (mvt_n[x].prise != prise_max);

  joue_coup(TabJeu, mvt_n, x, True);

  if (mvt_n[x].arr_y == 1)
    TabJeu[mvt_n[x].arr_x][mvt_n[x].arr_y] = DAME_N;

  affiche_tout(TabJeu);
  rafraichit();
  return 0;
}

int joue_b1(int TabJeu[12][12])
{
  LISTE_MVT mvt_b_tmp[256];
  int ptr_mvt_b_tmp[1];
  int TTJ[12][12];
  int i, j, k, pr, som;
  som = 0;
  cherche_coup(PION_B, TabJeu, mvt_b_tmp, ptr_mvt_b_tmp);
  pr = prise_max;

  for (k = 1; k <= ptr_mvt_b_tmp[0]; k++)
  {
    for (i = 0; i < 12; i++)
      for (j = 0; j < 12; j++)
        TTJ[i][j] = TabJeu[i][j];

    if ((pr == 0) || (mvt_b_tmp[k].prise == pr))
    {
      joue_coup(TTJ, mvt_b_tmp, k, False);

      if (mvt_b_tmp[k].arr_y == 10)
        TTJ[mvt_b_tmp[k].arr_x][mvt_b_tmp[k].arr_y] = DAME_B;

      som += eval_pos(TTJ);
    };
  };

  return som;
};

void joue_n1(int TabJeu[12][12])
{
  LISTE_MVT mvt_n_tmp[256];
  int ptr_mvt_n_tmp[1];
  int TTJ[12][12];
  int i, j, k, l, pr;
  
  /* Recherche des coups noirs */
  cherche_coup(PION_N, TabJeu, mvt_n_tmp, ptr_mvt_n_tmp);
  pr = prise_max;

  /* Simulation de chaque coup */
  for (k = 1; k <= ptr_mvt_n_tmp[0]; k++)
  {
    /* Copie du tableau de jeu */
    for (i = 0; i < 12; i++)
      for (j = 0; j < 12; j++)
        TTJ[i][j] = TabJeu[i][j];

    if ((pr == 0) || (mvt_n_tmp[k].prise == pr)) // pas de prise ou dernier coup, on simule le coup suivant
    {
      joue_coup(TTJ, mvt_n_tmp, k, False);

      if (mvt_n_tmp[k].arr_y == 1)
        TTJ[mvt_n_tmp[k].arr_x][mvt_n_tmp[k].arr_y] = DAME_N;

      mvt_n_tmp[k].ev_pos += joue_b1(TTJ);
    };
  };

  /* Liste des mouvements évalués */

  k = 0;

  l = 65535;

  for (i = 1; i <= ptr_mvt_n_tmp[0]; i++)
  {
    fprintf(stderr, "%d %c%d %c%d %c%d %d %d %d\n", i, mvt_n_tmp[i].dep_y + 64, mvt_n_tmp[i].dep_x, mvt_n_tmp[i].pr_y + 64, mvt_n_tmp[i].pr_x, mvt_n_tmp[i].arr_y + 64, mvt_n_tmp[i].arr_x, mvt_n_tmp[i].prec, mvt_n_tmp[i].prise, mvt_n_tmp[i].ev_pos);

    if ((mvt_n_tmp[i].ev_pos < l) && (mvt_n_tmp[i].prise == pr))
    {
      l = mvt_n_tmp[i].ev_pos;
      k = i;
    };
  };

  fprintf(stderr, "Je joue en %d : %c%d %c%d %d\n", k, mvt_n_tmp[k].dep_y + 64, mvt_n_tmp[k].dep_x, mvt_n_tmp[k].arr_y + 64, mvt_n_tmp[k].arr_x, mvt_n_tmp[k].ev_pos);

  joue_coup(TabJeu, mvt_n_tmp, k, True);

  if (mvt_n_tmp[k].arr_y == 1)
    TabJeu[mvt_n_tmp[k].arr_x][mvt_n_tmp[k].arr_y] = DAME_N;

  affiche_tout(TabJeu);
  rafraichit();
};

int joue_n(int TabJeu[12][12], LISTE_MVT mvt_n[256], int ptr_mvt_n[1])
{
  cherche_coup(PION_N, TabJeu, mvt_n, ptr_mvt_n);

  if (ptr_mvt_n[0] == 0)
  {
    partie_gagnee();
    return 1;
  };
  
  if (niveau == 0)
    return joue_n0(TabJeu, mvt_n, ptr_mvt_n); /* Random move */
  else
    joue_n1(TabJeu);
  
  affiche_tout(TabJeu);
  rafraichit();
  return 0;
}

void boucle_jeu(int TabJeu[12][12], LISTE_MVT mvt_b[256], int ptr_mvt_b[1], LISTE_MVT mvt_n[256], int ptr_mvt_n[1])
{
  while (True)
  {
    if (joue_b_interactif(TabJeu, mvt_b, ptr_mvt_b) != 0)
      break;

    if (joue_n(TabJeu, mvt_n, ptr_mvt_n) != 0)
      break;

    fprintf(stderr, "Score : %d\n", eval_pos(TabJeu));
  };

  getchar();

  return;
};

int main(int argc, char* argv[])
{
  int i, j;
  int TabJeu[12][12];
  LISTE_MVT mvt_b[256];
  LISTE_MVT mvt_n[256];
  int ptr_mvt_b[1];
  int ptr_mvt_n[1];

  if (init_fenetre() != 0)
  {
    fprintf(stderr, "Erreur : impossible d'ouvrir la fenêtre\n");
    return 1;
  };

  for (i = 0; i < 12; i++)
    for (j = 0; j < 12; j++)
      TabJeu[i][j] = 0;

  init_tabjeu(TabJeu);
  
  efface_tab_mvt(mvt_b, ptr_mvt_b);
  efface_tab_mvt(mvt_n, ptr_mvt_n);
  efface_tab_mvt(mvt_tmp, ptr_mvt_tmp);
  
  efface_fenetre(17);
  
  XSync(XDisp, True);
  
  affiche_tout(TabJeu);
  
  cherche_coup(PION_B, TabJeu, mvt_b, ptr_mvt_b);
  affiche_mvt_b(mvt_b, ptr_mvt_b);
  
  boucle_jeu(TabJeu, mvt_b, ptr_mvt_b, mvt_n, ptr_mvt_n);
  
  detruit_fenetre();
  
  return 0;
}
