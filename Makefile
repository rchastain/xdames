CC=gcc

LDLIBS=-lm -lX11
CFLAGS=-Wall -ggdb
XPATH=-L/usr/X11R6/lib

xdames: xdames.o xdames-disp.o
	$(CC) $(LDLIBS) $(XPATH) $(CFLAGS) xdames.o xdames-disp.o -o xdames
xdames.o: xdames.c
	$(CC) $(CFLAGS) -c xdames.c
xdames-disp.o: xdames-disp.c
	$(CC) $(CFLAGS) -c xdames-disp.c
